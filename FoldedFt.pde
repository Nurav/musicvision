import ddf.minim.*;

Minim minim;
AudioSource in;
String[] s = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
void setup() {
  size(512, 360);
  background(255);
  minim = new Minim(this);
  in = minim.getLineIn(minim.STEREO, 1000, 40000);
  strokeWeight(5);
}      

void draw() { 
  background(255);
  float[][] notes = new float[7][12];
  float[] folded_notes = new float[12];
  getNotes(1, 7, notes);
  for (int x = 0; x < notes.length; x++) {
    for (int y = 0; y < notes[x].length; y++) {
      //line(x * 65 + y*5, height, x * 65 + y*5, height - notes[x][y]);
      folded_notes[y] += pow(notes[x][y], 2) * 1/100;
    }
  }
  for (int i = 0; i < 12; i++) {
    line(i*5, height, i*5, height - folded_notes[i]);
  }
  /*int avg = nice(notes);
  for (int i = 0; i < notes.length; i++) {
    for (int y = 0; y < notes[i].length; i++) {
      if (notes[i][y] > avg * 3/2) {
        println("Octave " + i + " ");
        print(s[y]);
      } 
    }
  }*/
} 

float forierTranny(float frequency, AudioSource in) {
  float omegawd = TWO_PI * frequency;
  float real = 0;
  float imaginary = 0;
  for (int i = 0; i < in.bufferSize(); i++) {
    real += cos(omegawd * i / in.sampleRate()) * in.mix.get(i);
    imaginary += -sin(omegawd * i / in.sampleRate()) * in.mix.get(i);
  }
  float mag = sqrt(real * real + imaginary * imaginary);
  return mag;
}

void forierOctave(int octave, AudioSource in, float[] outArray){
  for (int i = 0; i < 12; i ++) {
    float noteFrequency = 440 * pow(2, (12 * octave + i - 48) / 12.0);
    outArray[i] = forierTranny(noteFrequency, in) * height * 0.005;
  }
}

void getNotes(int startOctave, int endOctave, float[][] outArray) {
  for (int i = startOctave; i <= endOctave; i++) {
    float[] notes = new float[12];
    forierOctave(i, in, notes);
    outArray[i - startOctave] = notes;
  }
}

int nice(float[][] arr) {
  int arrSize = arr.length * arr[0].length;
  int[] oneD = new int[arrSize];
  for (int i = 0; i < arr.length; i++) {
    for (int y = 0; y < arr[i].length; i++) {
      oneD[i * arr[i].length + y] = int(arr[i][y]); 
    }
  }
  mergeSort(oneD, 0, oneD.length - 1);
  return oneD[oneD.length / 2];
}

void mergeSort(int[] array, int low, int high) {
    if (high <= low) return;

    int mid = (low+high)/2;
    mergeSort(array, low, mid);
    mergeSort(array, mid+1, high);
    merge(array, low, mid, high);
}

void merge(int[] array, int low, int mid, int high) {
    int leftArray[] = new int[mid - low + 1];
    int rightArray[] = new int[high - mid];

    for (int i = 0; i < leftArray.length; i++)
        leftArray[i] = array[low + i];
    for (int i = 0; i < rightArray.length; i++)
        rightArray[i] = array[mid + i + 1];

    int leftIndex = 0;
    int rightIndex = 0;

    for (int i = low; i < high + 1; i++) {
        if (leftIndex < leftArray.length && rightIndex < rightArray.length) {
            if (leftArray[leftIndex] < rightArray[rightIndex]) {
               array[i] = leftArray[leftIndex];
               leftIndex++;
            } else {
                array[i] = rightArray[rightIndex];
                rightIndex++;
            }
        } else if (leftIndex < leftArray.length) {
            array[i] = leftArray[leftIndex];
            leftIndex++;
        } else if (rightIndex < rightArray.length) {
            array[i] = rightArray[rightIndex];
            rightIndex++;
        }
    }
}
