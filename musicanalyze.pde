  import processing.sound.*;
import java.util.Arrays;
import processing.serial.*;

Serial myPort;
int portNum = 0;


FFT fft;
Amplitude amp;
AudioIn inA, inB;

int bands = 64;
float bandwidth, cm;

int narrowing = 4;

float[] spectrum = new float[bands * narrowing];
float[] prev = new float[bands];
float[] highestAmps = new float[bands];


float prevAmp = 0, maxAmp = 0;

float smoothing = 32;

void setup() {
  size(1024, 500);//fullScreen();
  colorMode(HSB);
  bandwidth = width / bands;
  cm = 300.0 / bands;
  println(cm);
  println(Arrays.toString(Serial.list()));
  String portName = Serial.list()[portNum];
  myPort = new Serial(this, portName, 115200);

  // Create an Input stream which is routed into the Amplitude analyzer
  fft = new FFT(this, bands * narrowing);
  amp = new Amplitude(this);
  inA = new AudioIn(this, 0);
  inB = new AudioIn(this, 0);



  // start the Audio Input
  inA.start();
  inB.start();

  // patch the AudioIn

  fft.input(inA);
  amp.input(inB);

  strokeWeight(0);
}

float diff, max, maxIndex = 0, prevMaxIndex = 0;

float colorSmoothing = 20;

int red, green, blue;
String colString = "0,0,0";


int loopsSinceBeat = 0;
void draw() {
  background(10);
  fft.analyze(spectrum);
  max = 0;
  for (int i = 0; i < bands; i++) {
    float h = (spectrum[i] + ((smoothing - 1) * prev[i])) / smoothing;
    highestAmps[i] = max(h, highestAmps[i]);
    diff = h / highestAmps[i];
    if (diff > max) {
      max = diff;
      maxIndex = i;
    }
    prev[i] = h;
    fill(i * cm, 255, 255);
    rect(i * bandwidth, height - height * h / highestAmps[i], bandwidth, height);
  }
  float curAmp = amp.analyze();

  prevMaxIndex = (maxIndex + ((colorSmoothing - 1) * prevMaxIndex)) / colorSmoothing;
  //int beatColor = round(prevMaxIndex * cm);
  color beatColor = color(prevMaxIndex * cm, 255, 255);
  fill(beatColor);


  if (curAmp - prevAmp > .015) {
    red = beatColor >> 16 & 0xFF;
    green = beatColor >> 8 & 0xFF;
    blue = beatColor & 0xFF;
    colString = red + "," + green + "," + blue + "," + curAmp*100;
    println("Beat: " + colString);
    loopsSinceBeat = 0;
  } else {
    colString = "0,0,0";
    loopsSinceBeat++;
  }
  myPort.write(colString);

  prevAmp = (curAmp + ((smoothing - 1) * prevAmp)) / smoothing;
  if (prevAmp < .01 && highestAmps[0] > .01) {
    Arrays.fill(highestAmps, .003);
    println("resetting");
  }

  maxAmp = max(maxAmp, prevAmp);
  float size = width * prevAmp / maxAmp;
  rect((width - size) / 2, 0, size, 20);
}